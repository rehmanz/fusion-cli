from cement.utils.shell import Prompt

class GenericSelectionPrompt(Prompt):
    class Meta:
        numbered = True
        default = 'no'
        clear = False

    def read(self):
        return self.input

class GenericInputPrompt(Prompt):
    class Meta:
        clear = False

    def read(self):
        return self.input

def fetch_info_prompt(type_text, options):
    type = GenericSelectionPrompt(text="Select %s type" %type_text,
                                  options=options, clear=True).read()
    url = GenericInputPrompt(text="Provide %s URL: " %type_text).read()
    token = GenericInputPrompt(text="Enter %s access token: " %type_text).read()

    return {"type"  : type,
            "url"   : url,
            "token" : token
            }