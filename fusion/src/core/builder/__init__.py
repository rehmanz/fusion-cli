from importlib import import_module

from fusion.src.core.builder.base_builder import BuilderBaseClass

def builder_factory(type, *args, **kwargs):
    try:
        if "." in type:
            module_name, class_name = type.split(".", 1)
        else:
            module_name = type
            class_name = "%sBuilder" %type.capitalize()

        builder_module = import_module('fusion.src.core.builder.%s' %module_name, package='builder')
        builder_class = getattr(builder_module, class_name)
        instance = builder_class(*args, **kwargs)

        return instance
    except Exception as e:
        raise ValueError("BuilderFactory Exception: %s" %e)