import os

from setuptools import setup, find_packages

setup(name='fusion',
      version=os.popen('git describe').read().rstrip(),
      description='Fusion CLI',
      long_description='Universal CLI for Continuous Integration & Deployment Ecosystem',
      classifiers=[
            'Development Status :: 1 - Planning',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python :: 3.6',
            'Topic :: Build Tools',
      ],
      url='https://github.com/fusionci/fusion-cli',
      author='Zile Rehman',
      author_email='rehmanz@yahoo.com',
      license='MIT',
      include_package_data=True,
      packages=find_packages(exclude=['contrib', 'docs', 'tests']),
      setup_requires='pytest-runner',
      install_requires=[
            'requests',
            'pylibmc',
            'cement',
            'Jinja2',
            'colorlog',
            'pyYaml',
            'pytest',
            'pytest-runner'
      ],
      tests_require=['pytest'],
      python_requires='>=3.6',
      scripts=['fusion/bin/fusion'],
      zip_safe=False)