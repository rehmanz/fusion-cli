import os
import unittest

from pathlib import Path
from fusion.src.cli.controller import FusionApp
from fusion.src.core.config.user_config import UserConfigFactory, USER_CONFIG_DIR, USER_CONFIG_FILENAME

class UserConfigFactoryTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.user_config_file = os.path.expanduser("%s/%s" %(USER_CONFIG_DIR, USER_CONFIG_FILENAME))

    def test_create(self):
        with FusionApp() as app:
            app.run()
            UserConfigFactory(app).create()
        self.assertTrue(Path(self.user_config_file).is_file())

    def test_read_write_config(self):
        with FusionApp() as app:
            app.run()
            config = app.config
            config.read(self.user_config_file)
            for section in config.sections():
                app.log.info("  section: %s" %section)
                for option in config.options(section):
                    app.log.info("      %s : %s" %(option, config.get(section, option)))
                    config.set(section, option, value="foo")
                    app.log.info("      %s : %s" %(option, config.get(section, option)))

    def test_update_code_repo(self):
        with FusionApp() as app:
            app.run()
            UserConfigFactory(app).update(code_repo={
                "type"  : "github",
                "url"   : "https://www.github.com",
                "token" : "secret123"
            })

    def test_update_builder(self):
        with FusionApp() as app:
            app.run()
            UserConfigFactory(app).update(builder={
                "type"  : "circle",
                "url"   : "https://www.circleci.com",
                "token" : "secret123"
            })

    @classmethod
    def tearDownClass(self):
        pass

if __name__ == "__main__":
    unittest.main()