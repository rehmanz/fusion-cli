import sys

from cement.core.foundation import CementApp
from cement.ext.ext_colorlog import ColorLogHandler
from cement.core.controller import CementBaseController, expose
from fusion.src.core.builder import builder_factory

class FusionBaseController(CementBaseController):
    class Meta:
        label = 'base'
        description = "Fusion CLI interacts with software integration & delivery pipeline"

class InitController(CementBaseController):
    class Meta:
        label = 'init'
        description = "Initialize Environment"
        stacked_on = 'base'
        stacked_type = 'nested'
        arguments=[
            (['--type'], dict(help='Resource type', action='store', required=True)),
            (['--url'], dict(help='Resource URL', action='store', required=True)),
            (['--token'], dict(help='Access Token', action='store', required=True)),
        ]

    @expose(hide=True)
    def default(self):
        self.app.log.info("TODO: Prompt user for required configuration.")
        pass

    @expose(help="Initialize builder")
    def builder(self):
        type = self.app.pargs.type
        url = self.app.pargs.url
        token = self.app.pargs.token
        builder = builder_factory(type=type, app=self.app)
        builder.create(url=url, token=token)

    @expose(help="Initialize provisioner")
    def provisioner(self):
        self.app.log.info("#TODO: Initialize provisioner")

    @expose(help="Initialize code repository")
    def code_repository(self):
        self.app.log.info("#TODO: Initialize code repository")


class FusionApp(CementApp):
    class Meta:
        label = 'fusion'
        extensions = ['colorlog']
        log_handler = ColorLogHandler(colors={'DEBUG':    '',
                                              'INFO':     'green',
                                              'WARNING':  'yellow',
                                              'ERROR':    'red',
                                              'CRITICAL': 'yellow,bg_red'})
        handlers = [FusionBaseController, InitController]