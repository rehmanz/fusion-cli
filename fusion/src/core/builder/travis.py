import requests

from . import BuilderBaseClass
from fusion.src.core.config.user_config import UserConfigFactory

class TravisBuilder(BuilderBaseClass):
    def __init__(self, app):
        self.app = app
        BuilderBaseClass.__init__(self, self.app)

    def create(self, url, token):
        try:
            request_url = "https://%s/user" %url
            headers = {
                "Authorization": "token %s" %token,
                'Travis-API-Version': "3",
                'User-Agent': "Fusion App",
            }
            r = requests.request(method="GET", url=request_url, headers=headers)
            if r.status_code != 200:
                raise ValueError("TravisCIBuilder Exception: %s" %r.text)
            self.app.log.info("Travis CI connection confirmed.")

            # Update config
            user_config = UserConfigFactory(self.app)
            user_config.create()
            user_config.update(builder={
                "type"  : "travis",
                "url"   : request_url,
                "token" : token
            })


        except Exception as e:
            msg = "TravisCIBuilder Exception - create: %s" %e
            self.app.log.error(msg)
            raise ValueError(msg)