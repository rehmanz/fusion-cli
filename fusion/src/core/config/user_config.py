import os

from pathlib import Path
from jinja2 import Environment

USER_CONFIG_TEMPLATE_FILENAME = "user.config"
USER_CONFIG_FILENAME = "config"
USER_CONFIG_DIR = "~/.fusion"

class UserConfigFactory:
    def __init__(self, app):
        self.app = app
        self.user_config_file = os.path.expanduser("%s/%s" %(USER_CONFIG_DIR, USER_CONFIG_FILENAME))

    def create(self):
        """
        Create initial user config file if one does not exist already
        
        :return: None
        """
        try:
            if not Path(self.user_config_file).is_file():
                # Fetch user config template
                user_config_template = "%s/%s" %(os.path.dirname(os.path.realpath(__file__)),
                                                 USER_CONFIG_TEMPLATE_FILENAME)
                with open(user_config_template) as f:
                    user_config_data = f.read()

                # Create skeleton user config
                Path(os.path.expanduser(USER_CONFIG_DIR)).mkdir(exist_ok=True)
                with open(os.path.expanduser(self.user_config_file), "w") as f:
                    f.write(user_config_data)
        except Exception as e:
            self.app.log.error("User Config Exception - create: %s" %e)

    def update(self, code_repo={}, builder={}):
        """
        Update configuration

        :param builder:
        :param code_repo:
        :return:
        """
        try:
            if builder:
                self.__update_section(section="builder", options=builder)

            if code_repo:
                self.__update_section(section="code.repository", options=code_repo)
        except Exception as e:
            self.app.log.error("User Config Exception - update: %s" %e)

    def __update_section(self, section, options):
        if not self.app.config.has_section(section):
            self.app.config.add_section(section)

        for option in options:
            self.app.config.set(section, option, value=options[option])

        self.__fetch_existing_config()
        self.__overwrite_existing_config()

    def __fetch_existing_config(self):
        self.app.config.read(self.user_config_file)

        for section in self.app.config.sections():
            if ("mail.dummy" in section) or ("log.colorlog" in section):
                self.app.config.remove_section(section)

        for section in self.app.config.sections():
            for option in self.app.config.options(section):
                self.app.config.set(section, option, value=self.app.config.get(section, option))

    def __overwrite_existing_config(self):
        with open(self.user_config_file, "w") as f:
            self.app.config.write(f)