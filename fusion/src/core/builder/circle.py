import requests

from . import BuilderBaseClass
from fusion.src.core.config.user_config import UserConfigFactory

class CircleCIBuilder(BuilderBaseClass):
    def __init__(self, name, app):
        BuilderBaseClass.__init__(self, name, app)

    def create(self, type, url, token):
        try:
            pass
        except Exception as e:
            msg = "CircleCIBuilder Exception - create: %s" %e
            self.app.log.error(msg)
            raise ValueError(msg)