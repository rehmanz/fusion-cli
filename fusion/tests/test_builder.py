import os
import unittest

from fusion.src.cli.controller import FusionApp
from fusion.src.core.builder import builder_factory

class BuildTest(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        pass

    def test_builder_factory(self):
        with FusionApp() as app:
            app.run()
            builder = builder_factory(type="travis", app=app)
            builder.create(url=os.environ['TRAVIS_API_URL'],
                             token=os.environ['TRAVIS_API_TOKEN'])

    @classmethod
    def tearDownClass(self):
        pass

if __name__ == "__main__":
    unittest.main()