PYTHON_VERSION=python3

clean:
ifndef TRAVIS
	pip uninstall -y fusion
endif

validate:
ifdef TRAVIS
	virtualenv -p ${PYTHON_VERSION} .venv
	. .venv/bin/activate
endif
	pip install -r requirements.txt

compile:
ifdef TRAVIS
	pip install .
else
	pip install -e .
endif

test:
	python setup.py test
	fusion

package:
	rm -rf dist/*
	python setup.py sdist bdist_wheel

deploy:
	cloudsmith push python fusionci/smiley dist/*.whl

.PHONY: clean validate compile test package deploy