from abc import ABCMeta, abstractmethod

class BuilderBaseClass(metaclass=ABCMeta):
    """
    BuilderBaseClass is abstract base class. All other builders will extend from this class.

    """
    def __init__(self, app):
        self.app = app

    @abstractmethod
    def create(self, type, url, token):
        pass